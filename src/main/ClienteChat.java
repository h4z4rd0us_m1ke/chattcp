package main;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ClienteChat extends JFrame implements ActionListener {
	
	private static final long serialVersionUID = 1L;
	private Socket socket = null;
	DataInputStream flujoEntrada;
	DataOutputStream flujoSalida;
	private String nombre;
	private JScrollPane scrollPane1;
	public static JTextArea textArea;
	public static JTextField txtMensaje;
	public static JTextField txtInferior;
	private JButton btnEnviar;
	private JButton btnSalir;
	private boolean repetir;
	
	/**
	 * Create the frame.
	 */
	public ClienteChat(Socket s, String nombre) {
		setTitle("Conexion del cliente: "+nombre);
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 620, 470);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_izq = new JPanel();
		contentPane.add(panel_izq, BorderLayout.CENTER);
		panel_izq.setLayout(new BorderLayout(0, 5));
		
		JPanel panel_izq_ar = new JPanel();
		panel_izq.add(panel_izq_ar, BorderLayout.NORTH);
		panel_izq_ar.setLayout(new BorderLayout(0, 0));
		
		txtMensaje = new JTextField();
		txtMensaje.setMargin(new Insets(6, 4, 6, 4));
		panel_izq_ar.add(txtMensaje);
		txtMensaje.setColumns(10);
		
		textArea = new JTextArea();
		scrollPane1 = new JScrollPane(textArea);
		panel_izq.add(scrollPane1, BorderLayout.CENTER);
		
		JPanel panel_izq_ab = new JPanel();
		panel_izq.add(panel_izq_ab, BorderLayout.SOUTH);
		panel_izq_ab.setLayout(new BorderLayout(0, 0));
		
		txtInferior = new JTextField();
		panel_izq_ab.add(txtInferior, BorderLayout.CENTER);
		txtInferior.setEditable(false);
		txtInferior.setColumns(10);
		
		JPanel panel_der = new JPanel();
		panel_der.setAlignmentX(Component.RIGHT_ALIGNMENT);
		contentPane.add(panel_der, BorderLayout.EAST);
		panel_der.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		JPanel panel_botones = new JPanel();
		panel_der.add(panel_botones);
		panel_botones.setLayout(new GridLayout(0, 1, 0, 10));
		
		btnEnviar = new JButton("Enviar");
		panel_botones.add(btnEnviar);
		btnEnviar.setMargin(new Insets(4, 18, 4, 18));
		btnEnviar.setAlignmentY(0.0f);
		btnEnviar.setAlignmentX(0.5f);
		btnEnviar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				enviarMensaje();
			}
		});
		
		btnSalir = new JButton("Salir");
		panel_botones.add(btnSalir);
		btnSalir.setMargin(new Insets(4, 18, 4, 18));
		btnSalir.setAlignmentY(Component.TOP_ALIGNMENT);
		btnSalir.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSalir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String texto = "> Abandona el chat..." + nombre;
				try {
					flujoSalida.writeUTF(texto);
					flujoSalida.writeUTF("*");
					repetir = false;//Para salir del bucle
				}catch (IOException ioe) {
					ioe.printStackTrace();
				}
				System.exit(0);
			}
		});
		
		this.nombre=nombre;
		socket=s;
		
		//Mensaje de entrada en el chat
		try {
			flujoEntrada = new DataInputStream(socket.getInputStream());
			flujoSalida = new DataOutputStream(socket.getOutputStream());
			String texto = "> Entra en el chat... "+nombre;
			flujoSalida.writeUTF(texto);
		}catch (IOException e){
			System.out.println("Error de E/S");
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void enviarMensaje() {
		String texto = nombre+"> "+txtMensaje.getText();
		try {
			txtMensaje.setText("");
			flujoSalida.writeUTF(texto);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void ejecutar() {
		repetir=true;
		String texto = "";
		while(repetir) {
			try {
				texto = flujoEntrada.readUTF();
				textArea.setText(texto);
			}catch (IOException e) {
				//error cuando el servidor se cierra
				JOptionPane.showMessageDialog(null, "IMPOSIBLE CONECTAR CON EL SERVIDOR\n"
						+ e.getMessage(), "<<MENSAJE DE ERROR:2>>", JOptionPane.ERROR_MESSAGE);
				repetir=false;
			}
		}
	}
	
	/**
	 * Launch the application.
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		try {
			String host = "localhost";
			int puerto = 3830;
			
			Socket cliente = new Socket(host, puerto);
			ClienteChat frame = new ClienteChat(cliente, "Oscar");
			txtInferior.setText("Programa cliente iniciado..."+"Conectado a "+host+":"+puerto);
			frame.setVisible(true);
		}
		
		catch(ConnectException e){
			System.out.println("Error de conexion: No se ha podido conectar al servidor");
		}

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
