package main;
import java.io.*;
import java.net.*;

public class HiloServidor extends Thread {
	DataInputStream flujoEntrada;
	Socket socket=null;
	ServidorChat server;
	
	public HiloServidor(Socket s) {
		socket = s;
		try {
			flujoEntrada = new DataInputStream(
					socket.getInputStream());
		}catch (IOException e) {
			System.out.println("Error de E/S");
			e.printStackTrace();
		}
	}
	
	public void run() {
		ServidorChat.txtConexiones.setText(ServidorChat.STR_CONEXIONES
				+ ServidorChat.actuales);
		String texto = ServidorChat.textArea.getText();
		enviarMensajes(texto);
		
		while (true) {
			String cadena = "";
			try {
				cadena = flujoEntrada.readUTF();
				
				if (cadena.trim().equals("*")) {
					ServidorChat.actuales--;
					ServidorChat.txtConexiones.setText(ServidorChat.STR_CONEXIONES
							+ ServidorChat.actuales);
					break;
				}
				
				ServidorChat.textArea.append(cadena + "\n");
				texto = ServidorChat.textArea.getText();
				enviarMensajes(texto);
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}
	}
	
	/**
	 * Envia los mensajes del textArea a todos los clientes
	 * del chat.
	 * @param texto Contenido del textArea.
	 */
	public void enviarMensajes(String texto) {
		for (int i=0; i<ServidorChat.conexiones; i++) {
			Socket s1 = ServidorChat.getSocketTabla(i);
			try {
				DataOutputStream flujoSalida = new DataOutputStream(
						s1.getOutputStream());
				flujoSalida.writeUTF(texto);
			} catch (SocketException e) {
				//esta excepion ocurre cuando escribimos en el socket
				//de un cliente que ha finalizado.
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
}
