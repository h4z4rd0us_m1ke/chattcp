package main;

import java.io.*;
import java.net.*;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.EmptyBorder;

public class ServidorChat extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static ServerSocket servidor;
	private static final int PUERTO = 3830;
	public static int conexiones = 0;
	public static int actuales = 0;
	private static int maximo = 10;
	public static final String STR_CONEXIONES = "NUMERO DE CONEXIONES ACTUALES: ";
	public static final String STR_MAX_CONEXIONES = "N� MAXIMO DE CONEXIONES ESTABLECIDAS: "+maximo;
	private static Socket[] tabla = new Socket[maximo];
	public static JTextArea textArea;
	public static JTextField txtConexiones;
	public static JTextField txtInferior;
	private JScrollPane scrollPane1;
	private JButton btnSalir;
	
	/**
	 * Create the frame.
	 */
	public ServidorChat() {
		setTitle("Ventana del Servidor del Chat");
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		setBounds(100, 100, 620, 470);
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_izq = new JPanel();
		contentPane.add(panel_izq, BorderLayout.CENTER);
		panel_izq.setLayout(new BorderLayout(0, 5));
		
		JPanel panel_izq_ar = new JPanel();
		panel_izq.add(panel_izq_ar, BorderLayout.NORTH);
		panel_izq_ar.setLayout(new BorderLayout(0, 0));
		
		txtConexiones = new JTextField();
		panel_izq_ar.add(txtConexiones);
		txtConexiones.setEditable(false);
		txtConexiones.setText(STR_CONEXIONES+actuales);
		txtConexiones.setColumns(10);
		
		textArea = new JTextArea();
		scrollPane1 = new JScrollPane(textArea);
		panel_izq.add(scrollPane1, BorderLayout.CENTER);
		
		JPanel panel_izq_ab = new JPanel();
		panel_izq.add(panel_izq_ab, BorderLayout.SOUTH);
		panel_izq_ab.setLayout(new BorderLayout(0, 0));
		
		txtInferior = new JTextField();
		panel_izq_ab.add(txtInferior, BorderLayout.CENTER);
		txtInferior.setEditable(false);
		txtInferior.setColumns(10);
		
		JPanel panel_der = new JPanel();
		panel_der.setAlignmentX(Component.RIGHT_ALIGNMENT);
		contentPane.add(panel_der, BorderLayout.EAST);
		panel_der.setLayout(new FlowLayout(FlowLayout.CENTER, 25, 5));
		
		btnSalir = new JButton("Salir");
		btnSalir.setMargin(new Insets(5, 18, 5, 18));
		btnSalir.setAlignmentY(Component.TOP_ALIGNMENT);
		panel_der.add(btnSalir);
		btnSalir.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		btnSalir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					servidor.close();
				}catch (IOException ioe) {
					ioe.printStackTrace();
				}
				System.exit(0);
			}
		});
	}
	
	/**
	 * Launch the application.
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		servidor = new ServerSocket(PUERTO);
		ServidorChat frame = new ServidorChat();
		txtInferior.setText("Servidor iniciado...");
		frame.setVisible(true);
		
		//ESPERANDO CONEXIONES (MAXIMO 10)
		while (conexiones < maximo) {
			Socket s = new Socket();
			try {
				s = servidor.accept();//esperando al cliente
				tabla[conexiones] = s;
				txtInferior.setText("Cliente conectado desde el puerto: "+s.getPort());
				conexiones++;
				actuales++;
				HiloServidor hilo = new HiloServidor(s);
				hilo.start();
			}catch (SocketException se) {
				break;
			}
		}
		if (!servidor.isClosed()) {
			try {
				txtConexiones.setForeground(Color.red);
				txtConexiones.setText(STR_MAX_CONEXIONES);
				servidor.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Servidor finalizado.");
	}
	
	public static Socket getSocketTabla(int indice){
		return tabla[indice];
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
